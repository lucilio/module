# Hackeamos Module #
## Autoloading blocks ##

Blocks folder are a simple location to your blocks to live. From there thei will be automagically loaded when your code are imported (plugin is loaded or load_module.php are include/required to yout work).

For creating blocks use wp scripts. With updated npm (hence npx) installed open a terminal in blocks folder and type:
$ `npx @wordpress/create-block --namespace=my_namespace --category=text name-of-the-block`
$ `cd name-of-the-block`
$ `npm start`

Your new block should be now created, imported and monitoring change, ready to reflect your customizations on the fly.
