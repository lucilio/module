<?php
namespace Hackeamos\Module;
use Exception;
/**
 * Modules may be plugins or themes and may be used in both context without code adaptation
 * 1. provide safe references to assets and functions no matter the WordPress context
 * 2. load "include" scripts from includes folder and initialize its functions using WordPress hooks
 * Module class loads a module file or path into a singleton structure allowing access to any module globally
 */


class Module {

    private static $modules = array();

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function get_module( $module_path, $module_settings = array() ) {
        if( ! file_exists( $module_path ) ) {
            throw new Exception( sprintf( "Module file not found: %s"), $module_path );
        }
        if( ! is_dir( $module_path ) ) {
            $module_path = dirname( $module_path );
        }
        if ( ! array_key_exists( $module_path, self::$modules ) ) {
            self::$modules[ $module_path ] = new self( $module_path, $module_settings );
        }
        return self::$modules[ $module_path ];
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone()
    {
    }
    
    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    /**
     * is not allowed to call from outside to prevent from creating non controlled instances,
     * to use the singleton, you have to obtain the instance from Modules::get_module( $module_path ) instead
     */
    private function __construct( $module_path, $module_settings ) {
        $this->path = $module_path;
        $this->type = $this->is_plugin() ? "plugin" : "theme";
        $this->slug = basename( $module_path );
        $this->setup( $module_settings );
    }

    /**
     * set some basic module info grabbed from wp theme/plugin data
     */ 
    private function set_info() {
        if( $this->is_plugin() ) {
            $this->url = str_replace( WP_PLUGIN_DIR, WP_PLUGIN_URL, $this->path );
            if( ! function_exists( "get_plugins" ) ) {
                include( implode( DIRECTORY_SEPARATOR, array( ABSPATH , "wp-admin", "includes", "plugin.php" ) ) );
            }
            $plugins = get_plugins();
            $plugins_paths = array_keys( $plugins );
            $plugin_data = FALSE;
            while ( count( $plugins_paths ) AND $plugin_data === FALSE ) {
                $plugin_path = array_shift( $plugins_paths );
                if( strpos( WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . $plugin_path, $this->path ) === 0 ) {
                    $plugin_data = $plugins[ $plugin_path ];
                }
            }
            $this->version = $plugin_data["Version"];
            $this->data = $plugin_data;
        }
        else {
            $this->url = get_stylesheet_directory();
            $theme = wp_get_theme();
            $this->version = $theme["Version"];
        }
    }

    private function setup( $module_settings ) {
        $this->set_info();
        foreach( $module_settings as $settings_key => $settings ) {
            $this->include( $settings_key, $settings );
        }
        $this->load_blocks();
    }

    public function include( $include_name, $include_settings ) {
        $include_path = implode( DIRECTORY_SEPARATOR, array( $this->path, "includes", $include_name, "include.php" ) );
        if( file_exists( $include_path ) ) {
            do_action( "before_module_include_{$include_name}", $include_settings, $this );
            include $include_path;
            do_action( "module_include_{$include_name}", $include_settings, $this );
        }
        else {
            trigger_error( sprintf( "ignoring setting key '%s': could not find include path '%s'", $include_name, $include_path ), E_USER_WARNING );
        }
    }

    public function load_blocks() {
        foreach( glob( "{$this->path}/blocks/*/block.json" ) as $block_path ) {
            if( file_exists( $block_path ) ) {
                register_block_type_from_metadata( $block_path );
            }
        }
    }

    /**
     * incorporate some generic code allocated as includes (under {$module_path}/includes folder )
     * includes may be exchanged between modules without any modification
     * loaded includes and tis settings args are defined by __construct second parameter
     */

    /**
     * whether this instance is a WordPress plugin or not
     */
    public function is_plugin( $module_path = NULL ) {
        if( is_null( $module_path ) ) {
            $module_path = $this->path;
        }
        return strpos( $module_path, WP_PLUGIN_DIR ) === 0;
    }
}

function get_module( $module_path, $module_settings = array() ) {
    return Module::get_module( $module_path, $module_settings );
}


/*
      $module_slug = get_module_slug();
    if( is_dir( get_module_path( "languages" ) ) ) {
        if( get_module_type() == "plugin" ) {
            load_plugin_textdomain( $domain, false, dirname( plugin_basename( $module_filepath ) ) . '/languages' );
        }
    }
    $settings_path = get_module_path( "functions.php" );
    if( file_exists( $settings_path ) ) {
        require_once $settings_path;
    }
    $settings = apply_filters( "{$module_slug}_settings", array(); // loads function.php and triggers 
    $already_included = array();
    foreach( $settings as $module_include => $module_settings ) {
        $include_path = get_module_path( "includes/{$module_include}.php" );
        if( ! in_array( $module_include, $already_included ) AND file_exists( $include_path ) ) {
            do_action( get_module_hookname("before_module_include_{$module_include}"), $module_settings, $module_include );
            require_once( $include_path );
            do_action( get_module_hookname("after_module_include_{$module_include}"), $module_settings, $module_include );
            $already_included[] = $module_include;
        }
        else {
            trigger_error( "Couldn't load '{$module_include}'", E_USER_WARNING );
            if( ! file_exists( $include_path ) ) {
                trigger_error( "'{$include_path}' does not exist", E_USER_WARNING );
            }
        }
    }
    load_scripts();
}}
if ( ! function_exists("hackeamos_module_init") ):
global $_hackeamos_module_data;


function get_module_info( ) {
    if( ! isset( $_hackeamos_module_data ) OR ! is_array( $_hackeamos_module_data ) ) {
        $_hackeamos_module_data = array();
    }
    if( ! array_key_exists( __FILE__, $_hackeamos_module_data ) ) {
        if( ! function_exists( "get_plugins" ) ) {
            include( implode( DIRECTORY_SEPARATOR, array( ABSPATH , "wp-admin", "includes", "plugin.php" ) ) );
        }
        $text_domain = array_key_exists( "Text Domain", $plugin_data ) ? $plugin_data["Text Domain"] : basename( $plugin_data["path"] );
        $domain_path = array_key_exists( "Domain Path", $plugin_data ) ? $plugin_data["Domain Path"] : "languages";
        load_plugin_textdomain(
            $text_domain,
            FALSE,
            str_replace( WP_CONTENT_DIR, "", $plugin_data["path"] . DIRECTORY_SEPARATOR . trim( $domain_path, "/" ) )
        );
        $_hackeamos_module_data[ __FILE__ ] = array(
            "type" => "plugin",
            "path" => $plugin_data["path"],
            "url" => $plugin_data["url"],
            "version" => $plugin_data["Version"],
            "slug" => basename( $plugin_data["path"] ),
            "data" => $plugin_data,
        );
    }
    return $_hackeamos_module_data[ __FILE__ ];
}

function get_module_path( $path = "" ) {
    return get_module_info()["path"] . DIRECTORY_SEPARATOR . $path;
}

function get_module_url( $path = "" ) {
    return get_module_info()["url"] . DIRECTORY_SEPARATOR . $path;
}

function get_module_version( $path = "" ) {
    return get_module_info()["version"];
}

function get_module_slug() {
    return get_module_info()["slug"];
}

function get_module_data() {
    return get_module_info()["data"];
}
function get_module_type() {
    return get_module_info()["type"];
}
function get_module_hookname( $hookname ) {
    $hookname = str_replace( "../", "", trim( $hookname, "_" ) );
    return "{$module_slug}_{$hookname}";
}
function module_loaded( $set_value = NULL ) {

}

function load_scripts() {
    $scripts = array(
        "index.js",
        "admin-index.js",
        "style.css",
        "admin-style.css"
    );
    foreach( $scripts as $script_name ) {
        list( $name, $extension ) = explode( ".", $script_name );
        $script_path = get_module_path( "build/{$script_name}" );
        $script_url = get_module_url( "build/{$script_name}" );
        $asset_file_name = "build/" . $name . ( $extension == "css" ? ".css" : "" ) . ".asset.php";
        $asset_file_path = get_module_path( $asset_file_name );
        if( file_exists( $asset_file_path ) ){
            $asset_file = include( $asset_file_path );
            $version = $asset_file["version"];
            $dependencies = $asset_file["dependencies"];
        }
        else{
             $version = get_module_version();
             $dependencies = array();
        }
        if( file_exists( $script_path ) ) {
            $handle = "{$name}-{$extension}";
            $url = $script_url;
            $hook = strpos( $script_name, "admin-" ) === 0 ? "admin_enqueue_scripts" : "wp_enqueue_scripts";
            $loader = $extension == "js" ? "wp_enqueue_script" : "wp_enqueue_style";
            $where = $extension == "js" ? TRUE : "all";
            add_action( $hook, function( $screen )use( $loader, $handle, $url, $dependencies, $version, $where ){
                $loader( $handle, $url, $dependencies, $version, $where );
            } );
        }
    }
}



function hackeamos_module_init( $module_filepath ) {
    add_action( "init", function()use( $module_filepath ) {
        module_init( $module_filepath );
    } );
}
endif;
*/