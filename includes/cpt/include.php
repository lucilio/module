<?php
namespace Hackeamos\Modules\CPT;

function init( $settings, $module ) {
    add_action( "init", function( ) use( $settings ){
        foreach( $settings as $custom_post_type => $custom_post_type_settings ) {
            register_post_type( $custom_post_type, $custom_post_type_settings );
        }
    }, 10 );
}
add_action( "module_include_cpt", __NAMESPACE__ . "\\init", 10, 2 );


function __init() {
    register_post_type( "newsletter", array(
        "labels" => array(
            "name" => __("Newsletters"),
            "singular_name" => __("Newsletter")
        ),
        "description" => __("Mail messages to be sent to your audience"),
        "public" => TRUE,
        "show_in_rest" => TRUE,
        "menu_icon" => "dashicons-email-alt2",
        "supports" => array( "title", "editor", "revisions", "trackbacks", "author", "excerpt", "page-attributes", "thumbnail",
            "custom-fields", "post-formats" )
    ) );
    add_filter( "single_template", __NAMESPACE__ . "\\default_template" );
}
#add_action( 'init', __NAMESPACE__ . "\\init", 10 );


function default_template( $template ) {
    $post = get_post();

    $template_found = locate_template( array( 'single-newsletter.php' ) );

    if ( $post->post_type === "newsletter" AND $template_found !== $template ) {
        /*
         * This is a 'newsletter' post
         * AND a 'single newsletter template' is not found on
         * theme or child theme directories, so load it
         * from our plugin directory.
         */
        return get_module_path( 'templates/single-newsletter.php' );
    }
    else {
        die(var_export(compact("template", "template_found", "post")));
    }

    return $template;
}

function newsletter_block_category( $categories ) {
    $categories[] = array(
        "slug" => "newsletter",
        "title" => __("Newsletter")
    );
    return $categories;
}
add_filter( 'block_categories', __NAMESPACE__ . "\\newsletter_block_category", 10, 2 );