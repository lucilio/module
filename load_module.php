<?php
namespace Hackeamos\Module; // replace with your project namespace
/**
 * This file retains basic setup for main resources usefull in a WordPress project.
 * Headers bellow will make this module appear as a WP plugin but it can be also used
 *  as part of a theme by simpling importing.
 * Resources are loaded as extensions while mail data is availabel via get_module_* functions.
 */
if( ! function_exists( "\\Hackeamos\\Module\\get_module" ) ) {
    require_once( "hackeamos_module.php" );
}
use function \Hackeamos\Module\get_module;
/**
 * Plugin Name:       My Hackeamos Module
 * Plugin URI:        https://hackeamos.org/module
 * Description:       Base code for WordPress projects
 * Version:           0.1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Hackeamos.Org
 * Author URI:        https://hackeamos.org
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://hackeamos.org/module
 * Text Domain:       hkms
 * Domain Path:       /languages
 * Hackeamos Module:  1
 */
?>
<?php
/**
 * Here example of a custom post type declared here and executed under (imutable) extension cpt.
 * Args are the same of register_post_type plus some automations (like rest-api integration creating some settings and meta-boxes).
 * Note that this base module and its extensions are still in beta and highly pendant of documentation.
 */
$settings = array(
    "cpt" => array(
        "my_cpt" => array(
            "labels" => array(
                "name" => __("Custom Post Type Items"),
                "singular_name" => __("Custom Post Type Item")
            ),
            "description" => __("A simple custom post type example"),
            "public" => TRUE,
            "show_in_rest" => TRUE,
            "menu_icon" => "dashicons-smile",
            "supports" => array( "title", "editor", "comments", "revisions", "trackbacks", "author", "excerpt", "page-attributes", "thumbnail",
                "custom-fields", "post-formats" )
        )
    )
);
$module = get_module( __FILE__, $settings );
?>